﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTPatients.ApplictionStoreRepository
{
    using System.Web;

    using CTPatients.Model;

    public class ApplicationDataStoreSingleton
    {
        private readonly IList<Patient> patients;

        private ApplicationDataStoreSingleton()
        {
            patients = new List<Patient>();
        }

        private const string PatientStoreKey = "PatientStore";

        private static HttpApplicationStateBase GetApplicationContext()
        {
            return new HttpApplicationStateWrapper(HttpContext.Current.Application);
        }

        public IList<Patient> Patients { 
            get
            {
                if (patients == null) return Current.Patients;

                return patients;
            }
        } 

        public static ApplicationDataStoreSingleton Current
        {
            get
            {
                var con = GetApplicationContext();
                if (con != null)
                {
                    // if the session is accesed before it has been created for a user
                    // or the session dies or is abandonded the actual session object 
                    // has the chance to be null
                    var store = con.Get(PatientStoreKey) as ApplicationDataStoreSingleton;

                    if (store != null)
                    {
                        return store;
                    }

                    store = new ApplicationDataStoreSingleton();

                    con.Set(PatientStoreKey, store);

                    return store;
                }
                return new ApplicationDataStoreSingleton();
            }
        }

    }
}
