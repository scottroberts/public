﻿using CTPatients.Model;
using CTPatients.Repository;
using CTPatients.ApplictionStoreRepository;
using StructureMap.Configuration.DSL;

namespace CTPatients.InMemoryRepository.Ioc
{
    public class IoCRegistry : Registry
    {
        public IoCRegistry()
        {
            For<IRepository<Patient>>().Use<PatientRepository>();
        }
    }
}
