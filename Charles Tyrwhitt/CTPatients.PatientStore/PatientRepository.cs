﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CTPatients.Model;
using CTPatients.Repository;

namespace CTPatients.ApplictionStoreRepository
{
    using CTPatients.Repository.Criteria;

    public class PatientRepository : IRepository<Patient>
    {
        public PatientRepository(){}
       
        public void Add(Patient patient)
        {
            ApplicationDataStoreSingleton.Current.Patients.Add(patient);
        }

        public IQueryable<Patient> AsQueryable()
        {
            return ApplicationDataStoreSingleton.Current.Patients.AsQueryable();
        }

        public IEnumerable<Patient> Match(ICriteria<Patient> criteria)
        {
            return criteria.BuildQueryFrom(this);
        }
    }
}