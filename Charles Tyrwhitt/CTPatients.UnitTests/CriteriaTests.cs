﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CTPatients.UnitTests
{
    using System.Collections.Generic;
    using System.Linq;

    using CTPatients.Model;
    using CTPatients.Repository;
    using CTPatients.Repository.Criteria;

    using Moq;

    [TestClass]
    public class CriteriaTests
    {
        private Mock<IRepository<Patient>> mockRepository;

        private IQueryable<Patient> GetPatients()
        {
            var patients = new List<Patient>()
                {
                    new Patient { Name = "Wilma", Surname = "Flinstone", DateOfBirth = new DateTime(1960, 12, 01) },
                    new Patient { Name = "Fred", Surname = "Flinstone", DateOfBirth = new DateTime(1950, 12, 01) },
                    new Patient { Name = "Barney", Surname = "Rubble", DateOfBirth = new DateTime(1965, 12, 01) },
                    new Patient { Name = "Betty", Surname = "Rubble", DateOfBirth = new DateTime(1968, 12, 01) }
                };
            return patients.AsQueryable();
        }

        [TestInitialize]
        public void TestInitialize()
        {
            mockRepository = new Mock<IRepository<Patient>>();
            mockRepository.Setup(p => p.AsQueryable()).Returns(this.GetPatients());
        }
        
        [TestMethod]
        public void FindPatientExactCriteria_BuildQueryFrom_ReturnsMatch()
        {
            var patientToFind =new Patient { Name = "Fred", Surname = "Flinstone", DateOfBirth = new DateTime(1950, 12, 01) };
            var criteria = new FindPatientExactCriteria(patientToFind);

            var result = criteria.BuildQueryFrom(this.mockRepository.Object);

            Assert.IsTrue(result.FirstOrDefault() != null);
            Assert.AreEqual(result.First().Name, patientToFind.Name);
            Assert.AreEqual(result.First().Surname, patientToFind.Surname);
        }

        [TestMethod]
        public void FindPatientExactCriteria_BuildQueryFrom_ReturnsNull()
        {
            var patientToFind = new Patient { Name = "John", Surname = "Smith", DateOfBirth = new DateTime(1950, 12, 01) };
            var criteria = new FindPatientExactCriteria(patientToFind);

            var result = criteria.BuildQueryFrom(this.mockRepository.Object);

            Assert.IsTrue(result.FirstOrDefault() == null);
        }

        [TestMethod]
        public void GetAllPatientsCriteria_BuildQueryFrom_ReturnsPatients()
        {

            var criteria = new GetAllPatientsCriteria();
            var result = criteria.BuildQueryFrom(this.mockRepository.Object);

            Assert.IsTrue(result.Count() == this.GetPatients().Count());

        }
    }
}
