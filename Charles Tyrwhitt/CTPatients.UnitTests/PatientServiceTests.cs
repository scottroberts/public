﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CTPatients.UnitTests
{
    using System.Collections.Generic;
    using System.Linq;

    using CTPatients.Model;
    using CTPatients.Repository;
    using CTPatients.Repository.Criteria;
    using CTPatients.Services;

    [TestClass]
    public class PatientServiceTests
    {
        private Mock<IRepository<Patient>> mockRepository;

        private IPatientService patientService;

        private IQueryable<Patient> GetPatients()
        {
            var patients = new List<Patient>()
                               {
                                   new Patient{ Name = "Fred"},
                                   new Patient{ Name = "Wilma'"},
                                   new Patient{ Name = "Barney"},
                                   new Patient{ Name = "Betty" },
                                   new Patient{ Name = "Scott" }
                               };
            return patients.AsQueryable();
        }

        [TestInitialize]
        public void TestInitialize()
        {
            mockRepository = new Mock<IRepository<Patient>>();
            mockRepository.Setup(r => r.Match(It.IsAny<GetAllPatientsCriteria>())).Returns(this.GetPatients());
            
            patientService = new PatientService(mockRepository.Object);
        }

        [TestMethod]
        public void AddPatient_AddsPatientToRepository()
        {
            patientService.AddPatient(new Patient());

            mockRepository.Verify( a=>a.Add(It.IsAny<Patient>()),Times.AtLeastOnce);
        }

        [TestMethod]
        public void GetAllPatients_ReturnsAllPatients()
        {            
            var result = patientService.GetAllPatients();   
       
            Assert.IsTrue( result.Count() == 5);
        }

        [TestMethod]
        public void DoesPatientExist_NewPatient_ReturnsFalse()
        {

            mockRepository.Setup(r => r.Match(It.IsAny<FindPatientExactCriteria>())).Returns(new List<Patient>());
            var result = patientService.DoesPatientExist(new Patient { Name = "Bob" });

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void DoesPatientExist_ExistingPatient_ReturnsTrue()
        {
            mockRepository.Setup(r => r.Match(It.IsAny<FindPatientExactCriteria>())).Returns(new List<Patient> {new Patient{ Name = "Wilma" }});
            
            var result = patientService.DoesPatientExist(new Patient { Name = "Wilma" });

            Assert.IsTrue(result);
        }
    }
}
