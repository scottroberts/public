﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CTPatients.UnitTests
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;

    using CTPatients.MVC.ViewModels;

    [TestClass]
    public class ViewModelIntergrationTests
    {
        [TestMethod]
        public void EmptyPatientViewModel_DataValidationValidate_FailsValidation()
        {
            var patient = new PatientViewModel();
            var context = new ValidationContext(patient, serviceProvider: null, items: null);
            var results = new List<ValidationResult>();

            var isValid = Validator.TryValidateObject(patient, context, results, true);

            Assert.IsFalse(isValid);
        }

        [TestMethod]
        public void PatientNameLength_LessThanRequired_FailsValidation()
        {
            var patient = new PatientViewModel(){ Name="a", Surname="aa" };
            var context = new ValidationContext(patient, serviceProvider: null, items: null);
            var results = new List<ValidationResult>();

            var isValid = Validator.TryValidateObject(patient, context, results, true);

            Assert.IsFalse(isValid);
            Assert.IsTrue(results.Any(vr => vr.ErrorMessage == "Name must be 2 or more characters"));
        }

        [TestMethod]
        public void PatientSurnameLength_LessThanRequired_FailsValidation()
        {
            var patient = new PatientViewModel() { Name = "aaaa", Surname = "aa" };
            var context = new ValidationContext(patient, serviceProvider: null, items: null);
            var results = new List<ValidationResult>();

            var isValid = Validator.TryValidateObject(patient, context, results, true);

            Assert.IsFalse(isValid);
            Assert.IsTrue(results.Any(vr => vr.ErrorMessage == "Surname must be 3 or more characters"));
        }

        [TestMethod]
        public void PatientDateOfBirth_NotSet_FailsValidation()
        {
            var patient = new PatientViewModel() { Name = "aaaa", Surname = "aaaa"};
            var context = new ValidationContext(patient, serviceProvider: null, items: null);
            var results = new List<ValidationResult>();

            var isValid = Validator.TryValidateObject(patient, context, results, true);

            Assert.IsFalse(isValid);
            Assert.IsTrue(results.Any(vr => vr.ErrorMessage == "Please Enter A Date of Birth"));
        }

        [TestMethod]
        public void PatientDateOfBirth_InvalidFormat_FailsValidation()
        {
            var patient = new PatientViewModel() { Name = "aaaa", Surname = "aaaa", DateOfBirth = "11-11-1111" };
            var context = new ValidationContext(patient, serviceProvider: null, items: null);
            var results = new List<ValidationResult>();

            var isValid = Validator.TryValidateObject(patient, context, results, true);

            Assert.IsFalse(isValid);
            Assert.IsTrue(results.Any(vr => vr.ErrorMessage == "Invalid date format - date of birth needs to be in format yyyy-mm-dd"));
        }

        [TestMethod]
        public void PatientDateOfBirth_ValidFormat_PassesValidation()
        {
            var patient = new PatientViewModel() { Name = "aaaa", Surname = "aaaa", DateOfBirth = "1111-11-11" };
            var context = new ValidationContext(patient, serviceProvider: null, items: null);
            var results = new List<ValidationResult>();

            var isValid = Validator.TryValidateObject(patient, context, results, true);

            Assert.IsFalse(isValid);
            Assert.IsTrue(results.Any(vr => vr.ErrorMessage == "Invalid date format - date of birth needs to be in format yyyy-mm-dd"));
        }
    }
}
