﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CTPatients.Repository.Criteria
{
    using CTPatients.Model;

    public class FindPatientExactCriteria : ICriteria<Patient>
    {
        private readonly Patient patient;

        public FindPatientExactCriteria(Patient patient)
        {
            this.patient = patient;
        }

        public IEnumerable<Patient> BuildQueryFrom(IRepository<Patient> ds)
        {
            return
                ds.AsQueryable()
                  .Where(
                      p =>
                      p.Name == this.patient.Name && 
                      p.Surname == this.patient.Surname && 
                      p.DateOfBirth.Date == this.patient.DateOfBirth.Date);
        }
    }
}
