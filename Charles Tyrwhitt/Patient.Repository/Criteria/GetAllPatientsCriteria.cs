﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CTPatients.Repository.Criteria
{
    using CTPatients.Model;

    public class GetAllPatientsCriteria : ICriteria<Patient>
    {
        public IEnumerable<Patient> BuildQueryFrom(IRepository<Patient> ds)
        {
            return ds.AsQueryable().AsEnumerable();
        }
    }
}
