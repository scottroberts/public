﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTPatients.Repository.Criteria
{
    using CTPatients.Model;

    public interface ICriteria<TEntity> where TEntity : IEntity
    {
        IEnumerable<TEntity> BuildQueryFrom(IRepository<TEntity> ds);
    }
}
