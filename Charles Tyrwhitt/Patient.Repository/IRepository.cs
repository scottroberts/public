﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CTPatients.Model;

namespace CTPatients.Repository
{
    using CTPatients.Repository.Criteria;

    public interface IRepository<TEntity> where TEntity : IEntity
    {
        void Add(TEntity item);
/*        void Update(TEntity item);
        TEntity GetById(int Id);
        void Delete(TEntity item);
        */
        IQueryable<TEntity> AsQueryable();

        IEnumerable<TEntity> Match(ICriteria<TEntity> criteria);
    }
}
