﻿
using System.Collections.Generic;

namespace CTPatients.Services
{
    using CTPatients.Model;

    public interface IPatientService
    {
        void AddPatient(Patient patient);

        bool DoesPatientExist(Patient patient);

        IEnumerable<Patient> GetAllPatients();
    }
}
