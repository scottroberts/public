﻿namespace CTPatients.Services.Ioc
{
    using StructureMap.Configuration.DSL;

    public class IoCRegistry : Registry
    {
        public IoCRegistry()
        {
            For<IPatientService>().Use<PatientService>();
        }
    }
}
