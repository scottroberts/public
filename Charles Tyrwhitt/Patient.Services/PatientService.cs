﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CTPatients.Repository.Criteria;

namespace CTPatients.Services
{
    using CTPatients.Model;
    using CTPatients.Repository;

    public class PatientService : IPatientService
    {
        private readonly IRepository<Patient> patientStore;

        public PatientService(IRepository<Patient> patientStore)
        {
            this.patientStore = patientStore;
        }

        public void AddPatient(Patient patient)
        {
            patientStore.Add(patient);
        }

        public bool DoesPatientExist(Patient patient)
        {
            var criteria = new FindPatientExactCriteria(patient);
            return this.patientStore.Match(criteria).FirstOrDefault() != null;
        }

        public IEnumerable<Patient> GetAllPatients()
        {
            var criteria = new GetAllPatientsCriteria();
            return this.patientStore.Match(criteria);
        }
    }
}
