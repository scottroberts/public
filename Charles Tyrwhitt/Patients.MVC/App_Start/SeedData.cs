﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CTPatients.MVC.App_Start
{
    using CTPatients.Model;
    using CTPatients.Repository;
    using CTPatients.Services;

    using StructureMap;

    public static class SeedData
    {
        public static void SeedPatients()
        {
           var patientStore =  ObjectFactory.GetInstance<IRepository<Patient>>();
            patientStore.Add(
                new Patient { Name = "Wilma", Surname = "Flinstone", DateOfBirth = new DateTime(1960, 6, 28) });
            patientStore.Add(
                new Patient { Name = "Fred", Surname = "Flinstone", DateOfBirth = new DateTime(1950, 8, 21) });
            patientStore.Add(
                new Patient { Name = "Barney", Surname = "Rubble", DateOfBirth = new DateTime(1965, 2, 11) });
            patientStore.Add(
                new Patient { Name = "Betty", Surname = "Rubble", DateOfBirth = new DateTime(1968, 4, 10) });
        }
    }
}