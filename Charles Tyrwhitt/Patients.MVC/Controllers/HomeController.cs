﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CTPatients.MVC.Controllers
{
    using CTPatients.MVC.ViewModels;
    using CTPatients.MVC.ViewModels.Mappers;
    using CTPatients.Model;
    using CTPatients.Services;

    public class HomeController : Controller
    {
        private readonly IPatientService patientService;
        private readonly IDomainViewModelMapper<IEnumerable<Patient>, AllPatientsViewModel> patientsViewModelMapper;

        public HomeController(IPatientService patientService,             
                        IDomainViewModelMapper<IEnumerable<Patient>, AllPatientsViewModel> patientsViewModelMapper)
        {
            this.patientService = patientService;
            this.patientsViewModelMapper = patientsViewModelMapper;
        }

        public ActionResult Index()
        {
            var viewModel = patientsViewModelMapper.Map(patientService.GetAllPatients());

            return View(viewModel);
        }
    }
}
