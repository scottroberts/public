﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CTPatients.MVC.Controllers
{
    using CTPatients.MVC.ViewModels;
    using CTPatients.MVC.ViewModels.Mappers;
    using CTPatients.Model;
    using CTPatients.Services;

    public class PatientController : Controller
    {
        private readonly IPatientService patientService;
        private readonly IViewModelDomainMapper<PatientViewModel, Patient> patientViewModelDomainMapper;

        public PatientController(
            IPatientService patientService, 
            IViewModelDomainMapper<PatientViewModel, Patient> patientViewModelDomainMapper)
        {
            this.patientService = patientService;
            this.patientViewModelDomainMapper = patientViewModelDomainMapper;
        }

        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(PatientViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var patient = patientViewModelDomainMapper.Map(viewModel);
                patientService.AddPatient(patient);

                return RedirectToAction("Index", "Home");
            }

            return View(viewModel);
        }

    }
}
