﻿using System;
using System.Collections.Generic;

namespace CTPatients.MVC.ViewModelValidator
{
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    using CTPatients.MVC.ViewModels;
    using CTPatients.MVC.ViewModels.Mappers;
    using CTPatients.Model;
    using CTPatients.Services;

    using StructureMap;

    public class UniquePatientAttribute : ValidationAttribute
    {
        private IPatientService patientService;

        private IViewModelDomainMapper<PatientViewModel, Patient> patientViewModelDomainMapper;

        public UniquePatientAttribute()
        {
            try
            {
                this.patientService = ObjectFactory.GetInstance<IPatientService>();
                this.patientViewModelDomainMapper = ObjectFactory.GetInstance<IViewModelDomainMapper<PatientViewModel, Patient>>();
            }
            catch (Exception ex)
            {
            }
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if( value != null ) {
                var viewmodel = (PatientViewModel)value;

                var patient = patientViewModelDomainMapper.Map(viewmodel);

                if (this.patientService.DoesPatientExist(patient))
                {
                    return new ValidationResult("This patient already exists.  Please correct this patient.");                    
                }
            }

            return ValidationResult.Success;
        }
    }

    public class UniquePatientValidator : DataAnnotationsModelValidator<UniquePatientAttribute>
    {
        private readonly string errorMessage;

        public UniquePatientValidator(ModelMetadata metadata, ControllerContext context, UniquePatientAttribute attribute)
            : base(metadata, context, attribute)
        {
            errorMessage = attribute.ErrorMessage;
        }

        public override IEnumerable<ModelClientValidationRule> GetClientValidationRules()
        {
            ModelClientValidationRule validationRule = new ModelClientValidationRule();
            validationRule.ErrorMessage = errorMessage;
            validationRule.ValidationType = "Patient";
            validationRule.ValidationParameters.Add("UniquePatientAttribute", "");

            return new[] { validationRule };
        }
    }
}