﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CTPatients.MVC.ViewModels
{
    using CTPatients.Model;

    public class AllPatientsViewModel
    {
        public IEnumerable<PatientViewModel> Patients { get; set; }
    }
}