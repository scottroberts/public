﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTPatients.MVC.ViewModels.Mappers
{
    public interface IDomainViewModelMapper< in TDomain, out TViewModel>
    {
        TViewModel Map(TDomain model);
    }
}
