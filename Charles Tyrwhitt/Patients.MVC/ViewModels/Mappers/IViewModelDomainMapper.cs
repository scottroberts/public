﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTPatients.MVC.ViewModels.Mappers
{
    public interface IViewModelDomainMapper<in TViewModel, out TDomain>
    {
        TDomain Map(TViewModel model);
    }
}
