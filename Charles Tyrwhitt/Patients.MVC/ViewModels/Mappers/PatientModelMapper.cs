﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CTPatients.MVC.ViewModels.Mappers
{
    using CTPatients.Model;

    public class PatientModelMapper : IDomainViewModelMapper<Patient, PatientViewModel>
    {
        public PatientViewModel Map(Patient model)
        {
            return new PatientViewModel()
                       {
                           Name = model.Name,
                           Surname = model.Surname,
                           DateOfBirth = model.DateOfBirth.ToString("yyyy'-'MM'-'dd"),
                           Note = model.Note
                       };
        }
    }
}