﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CTPatients.MVC.ViewModels.Mappers
{
    using CTPatients.Model;

    public class PatientViewModelMapper : IViewModelDomainMapper<PatientViewModel, Patient>
    {
        public Patient Map(PatientViewModel model)
        {
            return new Patient
                              {
                                  Name = model.Name,
                                  Surname = model.Surname,
                                  DateOfBirth = DateTime.Parse(model.DateOfBirth),
                                  Note = model.Note
                              };
        }
    }
}