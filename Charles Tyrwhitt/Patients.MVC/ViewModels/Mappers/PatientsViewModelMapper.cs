﻿using CTPatients.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CTPatients.MVC.ViewModels.Mappers
{
    public class PatientsViewModelMapper : IDomainViewModelMapper<IEnumerable<Patient>, AllPatientsViewModel> 
    {
        public AllPatientsViewModel Map(IEnumerable<Patient> model)
        {
            return new AllPatientsViewModel()
            {
                Patients = model.Select(a => new PatientViewModel
                               {
                                   Name = a.Name,
                                   Surname = a.Surname,
                                   DateOfBirth = a.DateOfBirth.Date.ToString("yyyy'-'MM'-'dd"),
                                   Note = a.Note
                               }).AsEnumerable()
            };
        }
    }
}