﻿using System;

namespace CTPatients.MVC.ViewModels
{
    using System.ComponentModel.DataAnnotations;

    using CTPatients.MVC.ViewModelValidator;

    [UniquePatient]
    public class PatientViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Please Enter A Name")]
        [DataType(DataType.Text)]
        [Display(Name = "Name")]
        [MinLength(2, ErrorMessage = "Name must be 2 or more characters")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please Enter A Surname")]
        [DataType(DataType.Text)]
        [Display(Name = "Surname")]
        [MinLength(3, ErrorMessage = "Surname must be 3 or more characters")]
        public string Surname { get; set; }

        [Required(ErrorMessage = "Please Enter A Date of Birth")]
        [DataType(DataType.Date, ErrorMessage = "Please enter a date of birth")]
        [Display(Name = "Date of Birth")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "yyyy'-'MM'-'dd")]
        [RegularExpression("^(19|20)[0-9][0-9]-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$", ErrorMessage = "Invalid date format - date of birth needs to be in format yyyy-mm-dd")]
        public string DateOfBirth { get; set; }

        [DataType(DataType.MultilineText)]
        public string Note { get; set; }
    }
}