﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace TicketMaster.Tests
{
    public class WordManagerTests
    {
        private WordManager _wordManager;

        [SetUp]
        public void TestSetUp()
        {
            var wordParser = new WordParser();
            var wordCounter = new CumulativeWordCounter();
            _wordManager = new WordManager(wordParser, wordCounter);
        }

        [Test]
        public void WhenGivenTextReturnCorrectCountOfWords()
        {
            const string inputText = @"Variations (in) ""the"" (""operational"") definitions (""of how"") to count the word in this sentence (namely, ""a"", ""an"", ""the"")";
            const int expectedWordCount = 18;

            var wordManagerProcessResult = _wordManager.Process(inputText);
            
            Assert.That(wordManagerProcessResult.Words.Length.Equals(expectedWordCount));
        }

        [Test]
        public void WhenGivenTextReturnsListOfWords()
        {
            const string inputText = "Variations in the operational definitions of how to count the";

            string [] expectedListOfWords = new []{ "Variations","in", "the","operational","definitions","of","how","to","count","the" };

            var actualWordCountResult = _wordManager.Process(inputText);

            CollectionAssert.AreEqual(expectedListOfWords, actualWordCountResult.Words);
        }

        [Test]
        public void WhenGivenTextWithPunctuationIgnoresIt()
        {
            const string inputText = "Variations in, the.";

            string[] expectedListOfWords = new[] { "Variations", "in", "the"};

            var actualWordCountResult = _wordManager.Process(inputText);

            CollectionAssert.AreEqual(expectedListOfWords, actualWordCountResult.Words);
        }

        [Test]
        public void WhenGivenTextWithSpeachMarksIgnoresThem()
        {
            const string inputText = @"Variations ""in"" the";

            string[] expectedListOfWords = new[] { "Variations", "in", "the" };

            var actualWordCountResult = _wordManager.Process(inputText);

            CollectionAssert.AreEqual(expectedListOfWords, actualWordCountResult.Words);
        }

        [Test]
        public void WhenGivenTextWithBracketsIgnoresThem()
        {
            const string inputText = @"Variations (in) the";

            string[] expectedListOfWords = new[] { "Variations", "in", "the" };

            var actualWordCountResult = _wordManager.Process(inputText);

            CollectionAssert.AreEqual(expectedListOfWords, actualWordCountResult.Words);
        }

        [Test]
        public void WhenGivenConcatenatedWordCountsAsOne()
        {
            const string inputText = @"father-in-law is great";

            string[] expectedListOfWords = new[] { "father-in-law", "is", "great" };

            var actualWordCountResult = _wordManager.Process(inputText);

            CollectionAssert.AreEqual(expectedListOfWords, actualWordCountResult.Words);
        }

        [Test]
        public void AfterProcessingResultContainsCumulativeFrequency()
        {
            const string inputText = @"The Variations (in) ""the"" (""operational"") in (""the"")";

            IDictionary<string, int> expecteDictionary = new Dictionary<string, int>()
            {
                {"variations", 1},
                {"in", 2},
                {"the", 3},
                {"operational", 1}
            };

            var wordManagerProcessResult = _wordManager.Process(inputText);

            CollectionAssert.AreEqual(expecteDictionary.OrderBy(s=>s.Key), wordManagerProcessResult.CumlativeCount.OrderBy(s => s.Key));
        }

        [Test]
        public void testThe()
        {
            var bodyText = @"Variations in the operational definitions of how to count the words can occur(namely, what ""counts as"" a word, and which words ""don't count"" toward the total).However, especially since "+
                            @"the advent of widespread word processing, there is a broad consensus on these operational "+
                            @"definitions (and hence the bottom-line integer result). The consensus is to accept the text " +
                            @"segmentation rules generally found in most word processing software (including how word " +
                            @"boundaries are determined, which depends on how word dividers are defined). The first trait of " + 
                            @"that definition is that a space (any of various whitespace characters, such as a ""regular"" word "+
                            @"space, an em space, or a tab character) is a word divider. Usually a hyphen or a slash is, too. " +
                            @"Different word counting programs may give varying results, depending on the text segmentation " +
                            @"rule details, and on whether words outside the main text(such as footnotes, endnotes, or " +
                            @"hidden text) are counted. But the behavior of most major word processing applications is " +
                            @"broadly similar. " +
                            @"However, during the era when school assignments were done in handwriting or with " +
                            @"typewriters, the rules for these definitions often differed from today's consensus. Most " +
                            @"importantly, many students were drilled on the rule that ""certain words don't count"", "+
                            @"usuallyarticles(namely, ""a"", ""an"", ""the""), but sometimes also others, such as conjunctions (for "+
                            @"example, ""and"", ""or"", ""but"") and some prepositions(usually ""to"", ""of""). Hyphenated permanent "+
                            @"compounds such as ""follow-up""(noun) or ""long-term""(adjective) were counted as one word.To " +
                            @"save the time and effort of counting word-by-word, often a rule of thumb for the average " +
                            @"number of words per line was used, such as 10 words per line.These ""rules"" have fallen by the " +
                            @"wayside in the word processing era; the ""word count"" feature of such software (which follows " +
                            @"the text segmentation rules mentioned earlier) is now the standard arbiter, because it is largely " +
                            @"consistent(across documents and applications) and because it is fast, effortless, and costless " +
                            @"(already included with the application). " +
                            @"As for which sections of a document ""count"" toward the total(such as footnotes, endnotes, " +
                            @"abstracts, reference lists and bibliographies, tables, figure captions, hidden text), the person in " +
                            @"charge(teacher, client) can define their choice, and users(students, workers) can simply select" +
                            @"(or exclude) the elements accordingly, and watch the word count automatically update.";

            var actualWordCountResult = _wordManager.Process(bodyText);

        }
    }
}
