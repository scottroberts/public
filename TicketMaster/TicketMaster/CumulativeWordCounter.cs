﻿using System.Collections.Generic;
using System.Linq;

namespace TicketMaster
{
    public class CumulativeWordCounter : ICountWords
    {
        public IDictionary<string, int> GetCumulativeFrequency(string[] words)
        {
            var cumlativeCount = new Dictionary<string, int>();

            words.ToList().ForEach(s =>
            {
                if (!cumlativeCount.ContainsKey(s.ToLower()))
                {
                    cumlativeCount.Add(s.ToLower(), 1);
                }
                else
                {
                    cumlativeCount[s.ToLower()]++;
                }
            });

            return cumlativeCount;
        }
    }
}
