﻿using System.Collections.Generic;

namespace TicketMaster
{
    public interface ICountWords
    {
        IDictionary<string, int> GetCumulativeFrequency(string[] words);
    }
}
