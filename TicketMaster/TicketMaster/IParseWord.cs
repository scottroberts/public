﻿namespace TicketMaster
{
    public interface IParseWord
    {
        string Parse(string gword);
    }
}
