﻿using System.Collections.Generic;

namespace TicketMaster
{
    public class ProcessedWordsResult
    {
        public ProcessedWordsResult( string [] words, IDictionary<string, int> cumlativeCount )
        {
            CumlativeCount = cumlativeCount;
            Words = words;
        }

        public string [] Words { get; private set; }

        public IDictionary<string, int> CumlativeCount { get; private set; }
    }
}
