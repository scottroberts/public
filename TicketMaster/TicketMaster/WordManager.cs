using System.Collections.Generic;
using System.Linq;

namespace TicketMaster
{
    public class WordManager
    {
        private readonly IParseWord _wordParser;
        private readonly ICountWords _wordCounter;

        public WordManager( IParseWord wordParser, ICountWords wordCounter )
        {
            _wordCounter = wordCounter;
            _wordParser = wordParser;
        }

        public ProcessedWordsResult Process(string inputText)
        {
            var words = GetWords(inputText);

            var cumlativeWords = _wordCounter.GetCumulativeFrequency(words);

            return new ProcessedWordsResult(words, cumlativeWords);
        }


        private string [] GetWords(string inputText)
        {
            var words = inputText.Split(' ');

            for (long count = 0; count < words.Length; count++)
            {
                words[count] = _wordParser.Parse(words[count]);
            }

            return words;
        }
    }
}