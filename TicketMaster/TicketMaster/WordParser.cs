﻿namespace TicketMaster
{
    public class WordParser : IParseWord
    {
        public string Parse(string word)
        {
            return word.Replace(",", " ")
                .Replace(".", " ")
                .Replace("(", " ")
                .Replace(")", " ")
                .Replace(@"""", " ")
                .Replace("  ", " ")
                .Trim();
        }
    }
}
