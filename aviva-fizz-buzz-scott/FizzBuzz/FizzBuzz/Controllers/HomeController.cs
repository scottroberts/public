﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using FizzBuzz.ModelValidators;
using FizzBuzz.ViewModel;

namespace FizzBuzz.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            return View(new InputValueViewModel());
        }

        [FizzBuzzInputValidator]
        public ActionResult RunFizzBuzz(InputValueViewModel inputViewModel)
        {
            if (ModelState.IsValid)
            {
                var fizzBuzzService = new FizzBuzzService.FizzBuzzService();

                var viewModel = new FizzBuzzResultViewModel();

                viewModel.Results = fizzBuzzService.RunFizzBuzz(inputViewModel.Input);

                return View(viewModel);
            }

            return View("Index", inputViewModel);
        }
    }
}
