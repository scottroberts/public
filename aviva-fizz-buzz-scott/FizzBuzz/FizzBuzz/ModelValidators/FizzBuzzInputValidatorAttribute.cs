﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using FizzBuzz.ViewModel;

namespace FizzBuzz.ModelValidators
{
    public class FizzBuzzInputValidatorAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            var input = (InputValueViewModel) value;
            if (input.Input < 0)
                return false;

            if (input.Input > 1000)
                return false;

            return true;
        }
    }
}
