﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FizzBuzz.ViewModel
{
    public class FizzBuzzResultViewModel
    {
        public IList<string> Results { get; set; }
    }
}
