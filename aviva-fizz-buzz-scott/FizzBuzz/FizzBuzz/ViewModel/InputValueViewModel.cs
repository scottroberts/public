﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace FizzBuzz.ViewModel
{
    public class InputValueViewModel
    {
        [Required]
        [Range(1, 1000)]
        [Display(Name = "Input Value")]
        public int Input { get; set; }
    }
}
