﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzService
{
    public class FizzBuzzService
    {
        public IList<string> RunFizzBuzz(int maxCount)
        {
            var list = new List<string>();
            for (var counter = 1; counter <= maxCount; counter++)
            {
                list.Add(FizzBuzzValue(counter));
            }

            return list;
        }

        protected string FizzBuzzValue(int input)
        {
            var divisbleby3 = ((input)%3 == 0);
            var divisbleby5 = ((input)%5 == 0);

            if( divisbleby3 && divisbleby5)
            {
                return "fizzbuzz";
            }
            else if (divisbleby3)
            {
                return "fizz";
            } else if (divisbleby5)
            {
                return "buzz";
            }

            return input.ToString();
        }
    }
}
