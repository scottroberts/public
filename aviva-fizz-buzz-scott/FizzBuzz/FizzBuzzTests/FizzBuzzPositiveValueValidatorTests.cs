﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FizzBuzz.ModelValidators;
using FizzBuzz.ViewModel;

namespace FizzBuzzTests
{
    [TestClass]
    public class FizzBuzzPositiveValueValidatorTests
    {
        [TestMethod]
        public void FizzBuzzPositiveValueValidator_UserEntersPositiveInteger_ReturnsTrue()
        {

            var testValue = new InputValueViewModel() { Input = 1 };

            var positiveIntegerValidator = new FizzBuzzInputValidatorAttribute();

            var expectedAnswer = true;

            var actualAnswer = positiveIntegerValidator.IsValid(testValue);

            Assert.IsTrue(expectedAnswer == actualAnswer);
        }

        [TestMethod]
        public void FizzBuzzPositiveValueValidator_UserEntersNegativeInteger_ReturnsFalse()
        {

            var testValue = new InputValueViewModel() { Input = -1 };

            var positiveIntegerValidator = new FizzBuzzInputValidatorAttribute();

            var expectedAnswer = false;

            var actualAnswer = positiveIntegerValidator.IsValid(testValue);

            Assert.IsTrue(expectedAnswer == actualAnswer);
        }

        [TestMethod]
        public void FizzBuzzPositiveValueValidator_UserEntersPositiveIntegerBetween1and1000_ReturnsTrue()
        {

            var testValue = new InputValueViewModel() { Input = 1 };

            var positiveIntegerValidator = new FizzBuzzInputValidatorAttribute();

            var expectedAnswer = true;

            var actualAnswer = positiveIntegerValidator.IsValid(testValue);

            Assert.IsTrue(expectedAnswer == actualAnswer);
        }


        [TestMethod]
        public void FizzBuzzPositiveValueValidator_UserEntersPositiveIntegerOver1000_ReturnsFalse()
        {

            var testValue = new InputValueViewModel() { Input = 1001 };

            var positiveIntegerValidator = new FizzBuzzInputValidatorAttribute();

            var expectedAnswer = false;

            var actualAnswer = positiveIntegerValidator.IsValid(testValue);

            Assert.IsTrue(expectedAnswer == actualAnswer);
        }


    }
}
