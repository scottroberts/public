﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FizzBuzzService;

namespace FizzBuzzTests
{
    [TestClass]
    public class FizzBuzzServiceTests
    {
        [TestMethod]
        public void FizzBuzzService_WhenFizzBuzzGoIsCalled_ReturnList()
        {
            var fizzBuzzService = new FizzBuzzService.FizzBuzzService();

            var result = fizzBuzzService.RunFizzBuzz(1);

            Assert.IsInstanceOfType(result, typeof( IList<string>));
        }

        [TestMethod]
        public void FizzBuzzService_WhenFizzBuzzGoIsCalledWithValue_ReturnsListCorrectNumberofItems()
        {
            var fizzBuzzService = new FizzBuzzService.FizzBuzzService();

            var inputValue = 15;

            var result = fizzBuzzService.RunFizzBuzz(inputValue);

            Assert.IsTrue( result.Count == inputValue );            
        }

        [TestMethod]
        public void FizzBuzzService_WhenFizzBuzzGoIsCalledAndHasValuesDivisibleBy3_ReturnsListWithWordFizzInIt()
        {
            var fizzBuzzService = new FizzBuzzService.FizzBuzzService();

            var inputValue = 15;

            var result = fizzBuzzService.RunFizzBuzz(inputValue);

            Assert.IsTrue(result.Count == inputValue);
            Assert.IsTrue(result[2] == "fizz");
            Assert.IsTrue(result[0] == "1");
        }

        [TestMethod]
        public void FizzBuzzService_WhenFizzBuzzGoIsCalledAndHasValuesDivisibleBy5_ReturnsListWithWordBuzzInIt()
        {
            var fizzBuzzService = new FizzBuzzService.FizzBuzzService();

            var inputValue = 15;

            var result = fizzBuzzService.RunFizzBuzz(inputValue);

            Assert.IsTrue(result.Count == inputValue);
            Assert.IsTrue(result[4] == "buzz");
            Assert.IsTrue(result[0] == "1");
        }

        [TestMethod]
        public void FizzBuzzService_WhenFizzBuzzGoIsCalledAndHasValuesDivisibleBy3and5_ReturnsListWithWordFIzzBuzzInIt()
        {
            var fizzBuzzService = new FizzBuzzService.FizzBuzzService();

            var inputValue = 15;

            var result = fizzBuzzService.RunFizzBuzz(inputValue);

            Assert.IsTrue(result.Count == inputValue);
            Assert.IsTrue(result[14] == "fizzbuzz");
            Assert.IsTrue(result[0] == "1");
        }
    }
}
