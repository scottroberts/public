﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FizzBuzz.Controllers;
using FizzBuzz.ViewModel;
using System.Web.Mvc;

namespace FizzBuzzTests
{
    [TestClass]
    public class HomeControllerTests
    {
        [TestMethod]
        public void HomeController_InputPostResult_HasList()
        {
            var homeController = new HomeController();

            var result = homeController.RunFizzBuzz(new InputValueViewModel{ Input = 15 });

            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.IsInstanceOfType(((ViewResult)result).Model, typeof(FizzBuzzResultViewModel));
        }
    }
}
