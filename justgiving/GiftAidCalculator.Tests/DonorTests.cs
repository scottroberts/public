﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GiftAidServices;

namespace GiftAidCalculator.Tests
{
    using Moq;

    [TestFixture]
    public class DonorTests
    {
        [Test]
        public void CalculateGiftAidUsingCurrentTaxRate_Returns_AmountForCharity()
        {
            var mockTaxCalculator = new Mock<ITaxCalculator>();
            var mockDonation = new Mock<IDonation>();

            mockTaxCalculator.Setup(tc => tc.GetRatio()).Returns(0.25M);
            mockDonation.SetupGet(d => d.Amount).Returns(10.0M);

            var expectedResult = new GiftAid { Amount = 2.5M };

            var giftAidCalculator = new GiftAidService(mockTaxCalculator.Object);

            var actualResult = giftAidCalculator.Calculate(mockDonation.Object);

            Assert.AreEqual(expectedResult.Amount, actualResult.Amount);
        }

        [Test]
        public void RoundGiftAidToTwoDecimalPlaces()
        {
            var rounder = new GiftAidRounder(2);

            var expectedValue = new GiftAid { Amount = 1.32M };

            var giftAidAmount = new GiftAid { Amount = 1.316M };

            var actualResult = rounder.Round( giftAidAmount );

            Assert.AreEqual(expectedValue.Amount, actualResult.Amount);

        }
    }
}
