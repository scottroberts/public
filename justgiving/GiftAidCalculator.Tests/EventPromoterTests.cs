﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GiftAidServices;

namespace GiftAidCalculator.Tests
{
    using Moq;

    using NUnit.Framework;

    [TestFixture]
    public class EventPromoterTests
    {
        [Test]
        public void SwimmingSupplementIsAppliedToGiftAidCorrectly()
        {
            var mockEvent = new Mock<IGiftEvent>();
            var mockGiftAid = new Mock<IGiftAid>();

            mockGiftAid.SetupGet(gf => gf.Amount).Returns(100M);
            mockEvent.SetupGet(e => e.Name).Returns("swimming");
            mockGiftAid.SetupGet(gf => gf.GiftEvent).Returns(mockEvent.Object);

            var expectedAmount = 3M;

            var swimmingSupplement = new SwimmingSupplement(){ Amount = 3M };

            var actualAmount = swimmingSupplement.Get(mockGiftAid.Object);

            Assert.AreEqual(expectedAmount, actualAmount);
        }

        [Test]
        public void RunningSupplementIsAppliedToGiftAidCorrectly()
        {
            var mockEvent = new Mock<IGiftEvent>();
            var mockGiftAid = new Mock<IGiftAid>();

            mockGiftAid.SetupGet(gf => gf.Amount).Returns(100);
            mockEvent.SetupGet(e => e.Name).Returns("running");
            mockGiftAid.SetupGet(gf => gf.GiftEvent).Returns(mockEvent.Object);

            var expectedAmount = 5M;

            var swimmingSupplement = new RunningSupplement() { Amount = 5M };

            var actualAmount = swimmingSupplement.Get(mockGiftAid.Object);

            Assert.AreEqual(expectedAmount, actualAmount);
        }

        [Test]
        public void RunningSupplementIsAppliedToSwimmingGiftAidReturnsZero()
        {
            var mockEvent = new Mock<IGiftEvent>();
            var mockGiftAid = new Mock<IGiftAid>();

            mockGiftAid.SetupGet(gf => gf.Amount).Returns(100M);
            mockEvent.SetupGet(e => e.Name).Returns("swimming");
            mockGiftAid.SetupGet(gf => gf.GiftEvent).Returns(mockEvent.Object);

            var expectedAmount = 0M;

            var runningSupplement = new RunningSupplement() { Amount = 3M };

            var actualAmount = runningSupplement.Get(mockGiftAid.Object);

            Assert.AreEqual(expectedAmount, actualAmount);            
        }


        [Test]
        public void SupplementCalculatorGetsCorrectSupplementForSwimmingGiftAid()
        {
            var swimmingSupplement = new SwimmingSupplement() { Amount = 3M };
            var runningSupplement = new RunningSupplement() { Amount = 5M };
            var mockEvent = new Mock<IGiftEvent>();
            var mockGiftAid = new Mock<IGiftAid>();

            mockGiftAid.SetupGet(gf => gf.Amount).Returns(100m);
            mockEvent.SetupGet(e => e.Name).Returns("swimming");
            mockGiftAid.SetupGet(gf => gf.GiftEvent).Returns(mockEvent.Object);
   
            var expectedResult = 3M;

            var supplements = new List<ISupplement>();
            supplements.Add(swimmingSupplement);
            supplements.Add(runningSupplement);

            var supplementCalculator = new SupplementCalculator(supplements);

            var actualResult = supplementCalculator.Get(mockGiftAid.Object);

            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void SupplementCalculatorGetsCorrectSupplementForNonEventGiftAid()
        {
            var swimmingSupplement = new SwimmingSupplement() { Amount = 3M };
            var runningSupplement = new RunningSupplement() { Amount = 5M };
            var mockEvent = new Mock<IGiftEvent>();
            var mockGiftAid = new Mock<IGiftAid>();

            mockGiftAid.SetupGet(gf => gf.Amount).Returns(100m);
            mockEvent.SetupGet(e => e.Name).Returns("");
            mockGiftAid.SetupGet(gf => gf.GiftEvent).Returns(mockEvent.Object);

            var expectedResult = 0M;

            var supplements = new List<ISupplement>();
            supplements.Add(swimmingSupplement);
            supplements.Add(runningSupplement);

            var supplementCalculator = new SupplementCalculator(supplements);

            var actualResult = supplementCalculator.Get(mockGiftAid.Object);

            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}
