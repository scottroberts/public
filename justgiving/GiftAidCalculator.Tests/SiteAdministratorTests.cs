﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GiftAidServices;

namespace GiftAidCalculator.Tests
{
    using GiftAidServices;

    using Moq;

    using NUnit.Framework;

    [TestFixture]
    public class SiteAdministratorTests
    {
        [Test]
        public void ReadTaxRateFromStore()
        {
            var mockTaxStore = new Mock<ITaxStore>();

            mockTaxStore.Setup(ts => ts.GetRate()).Returns(20M);

            var taxCalculator = new TaxCalculator(mockTaxStore.Object);

            const decimal expectedResult = 0.25M;
            var actualResult = taxCalculator.GetRatio();

            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void CalulateGiftAidUsingRateInStore()
        {
            var mockTaxStore = new Mock<ITaxStore>();
            var mockDonation = new Mock<IDonation>();

            mockTaxStore.Setup(ts => ts.GetRate()).Returns(20M);
            mockDonation.SetupGet(d => d.Amount).Returns(10.0M);

            var expectedResult = new GiftAid { Amount = 2.5M };

            var taxCalculator = new TaxCalculator(mockTaxStore.Object);

            var giftAidCalculator = new GiftAidService(taxCalculator);

            var actualResult = giftAidCalculator.Calculate(mockDonation.Object);

            Assert.AreEqual(expectedResult.Amount, actualResult.Amount);
        }
  


    }


}
