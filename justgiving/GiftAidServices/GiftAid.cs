﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GiftAidServices
{
    public class GiftAid : IGiftAid
    {
        public GiftAid()
        {
            GiftEvent = new GiftEvent { Name = string.Empty };
        }
        public decimal Amount { get; set; }

        public decimal GetRoundedAmount(int places)
        {
            return Math.Round(this.Amount, places);
        }

        public IGiftEvent GiftEvent{get;set;}
    }
}
