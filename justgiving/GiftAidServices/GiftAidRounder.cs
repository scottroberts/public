﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GiftAidServices
{
    public class GiftAidRounder
    {
        private readonly int value;

        public GiftAidRounder(int value)
        {
            this.value = value;
        }

        public IGiftAid Round(IGiftAid giftAid)
        {
            return new GiftAid { Amount = Math.Round(giftAid.Amount, value) };
        }



    }
}
