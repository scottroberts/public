﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GiftAidServices
{
    public class GiftAidService : IGiftAidService
    {
        private readonly ITaxCalculator taxCalculator;

        public GiftAidService(ITaxCalculator taxCalculator)
        {
            this.taxCalculator = taxCalculator;
        }

        public IGiftAid Calculate(IDonation donation)
        {
            var giftAid = new GiftAid { Amount = donation.Amount * this.taxCalculator.GetRatio()};

            return giftAid;
        }
    }
}
