﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GiftAidServices
{
    public interface IGiftAid
    {
        IGiftEvent GiftEvent{ get; set; }

        decimal Amount { get; set; }
        decimal GetRoundedAmount(int places);
    }
}
