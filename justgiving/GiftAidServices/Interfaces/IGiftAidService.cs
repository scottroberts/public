﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GiftAidServices
{
    public interface IGiftAidService
    {
        IGiftAid Calculate(IDonation donation);
    }
}
