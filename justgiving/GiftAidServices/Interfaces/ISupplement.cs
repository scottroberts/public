﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GiftAidServices
{
    public interface ISupplement
    {
        decimal Amount { get; set; }
        decimal Get(IGiftAid giftAid);
    }
}
