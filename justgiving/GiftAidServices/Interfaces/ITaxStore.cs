﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GiftAidServices
{
    public interface ITaxStore
    {
        decimal GetRate();
    }
}
