﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GiftAidServices
{
    public class RunningSupplement: ISupplement
    {
        private decimal ApplySupplement(decimal giftAmount)
        {
            return (giftAmount / 100) * Amount;
        }
        
        public decimal Get(IGiftAid giftAid)
        {
            if (giftAid.GiftEvent.Name == "running")
            {
                return this.ApplySupplement(giftAid.Amount);
            }
            return 0M;
        }

        public decimal Amount{get; set; }
    }
}
