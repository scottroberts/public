﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GiftAidServices
{
    public class SupplementCalculator
    {
        private readonly IList<ISupplement> supplements;

        public SupplementCalculator(IList<ISupplement> supplements)
        {
            this.supplements = supplements;
        }

        public decimal Get(IGiftAid giftAid)
        {
            decimal result = 0.0M;

            foreach (var supplement in this.supplements)
            {
                result += supplement.Get(giftAid);
            }

            return result;
        }

    }
}
