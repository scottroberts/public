﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GiftAidServices
{
    public class TaxCalculator : ITaxCalculator
    {
        private ITaxStore store;

        public TaxCalculator(ITaxStore store)
        {
            this.store = store;
        }

        public decimal GetRatio()
        {
            return this.store.GetRate() / (100 - this.store.GetRate());
        }
    }
}
